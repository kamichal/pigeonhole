# Pigeonhole

`pytest-pigeonhole` is a `pytest` plugin that adds a terminal-summary in
whose outcomes are split along given fixture value.

It can be helpful if use parametrized fixtures with larger scope.


[![pipeline status](https://gitlab.com/kamichal/pigeonhole/badges/master/pipeline.svg)]
(https://gitlab.com/kamichal/pigeonhole/)
[![python versions](https://img.shields.io/pypi/pyversions/pytest-pigeonhole.svg)]
(https://pypi.org/project/pytest-pigeonhole/)
[![package version](https://img.shields.io/pypi/v/pytest-pigeonhole.svg)]
(https://pypi.org/project/pytest-pigeonhole/)
[![development status](https://img.shields.io/pypi/status/pytest-pigeonhole.svg)]
(https://gitlab.com/kamichal/pigeonhole/)

## Installation

```
pip install pytest-pigeonhole
```
No dependencies (besides `pytest` itself).


## Usage
One of the folowing:

1. Add `--pigeonhole given_fixture_name` parameter to 
your pytest run. That's all.

2. Or such a statement to `pytest.ini` or `tox.ini`:
```
[pytest]
addopts = --pigeonhole given_fixture_name
```

3. Alternatively dd such code to your `conftest.py`:

``` python
from pigeonhole.plugin import configure_pigeonhole

def pytest_configure(config):
    configure_pigeonhole(config, "given_fixture_name")
```
Each of above gives the same result.

## Usage example:

``` python
# conftest.py
import pytest

@pytest.fixture(params=["one", "two", 23, False])
def that_fixture(request):
    return request.param

@pytest.fixture(params=range(3))
def int_fixture(request):
    return request.param

```

``` python
# test_usage.py
import pytest

@pytest.fixture
def faulty_setup(that_fixture):
    assert not that_fixture

def test_passes(that_fixture):
    pass

def test_int_passes(int_fixture):
    if not int_fixture:
        pytest.skip()

def test_fail(that_fixture, int_fixture):
    assert int_fixture

def test_errors(faulty_setup):
    pass

```

Calling `pytest --pigeonhole that_fixture` adds such a terminal-report:

```
 ========== Pigeonhole: that_fixture =========
 'one' | p: 3    | f: 1    | s: 0    | e: 1   
 'two' | p: 3    | f: 1    | s: 0    | e: 1   
 23    | p: 3    | f: 1    | s: 0    | e: 1   
 False | p: 4    | f: 1    | s: 0    | e: 0   
 N/A   | p: 2    | f: 0    | s: 1    | e: 0   
```

Or call: `pytest --pigeonhole int_fixture` gives:

```
=========== Pigeonhole: int_fixture ==========
 0   | p: 0    | f: 4    | s: 1    | e: 0
 1   | p: 5    | f: 0    | s: 0    | e: 0
 2   | p: 5    | f: 0    | s: 0    | e: 0
 N/A | p: 5    | f: 0    | s: 0    | e: 3
```

The `N/A` shows counts for tests for whose the given fixture was not involved. 
