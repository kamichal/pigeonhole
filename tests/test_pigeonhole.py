import pytest
from collections import namedtuple

CallOption = namedtuple("CallOption", "pytest_call_args, conftest_contents")

CALL_OPTS = ["cmdline", "by_conftest"]


def make_runner(testdir, conftest, call_args):
    def run_pytest(py_file, expected_lines, expected_outcomes):
        testdir.makeconftest(conftest)
        testdir.makepyfile(py_file)
        result = testdir.runpytest(*call_args)
        result.assert_outcomes(**expected_outcomes)
        result.stdout.fnmatch_lines(expected_lines)
    return run_pytest


@pytest.fixture(params=CALL_OPTS)
def call_method(request):
    return request.param


@pytest.fixture
def that_runner(testdir, call_method):
    default_conftest = """
        import pytest

        @pytest.fixture(params=["one", "two", 23, False])
        def that_fixture(request):
            return request.param

        @pytest.fixture(params=range(3))
        def int_fixture(request):
            return request.param
        """

    if call_method == "cmdline":
        call_args = ["--pigeonhole", "that_fixture"]
        conftest = default_conftest

    else:
        call_args = []
        conftest = default_conftest + """
        from pigeonhole.plugin import configure_pigeonhole

        def pytest_configure(config):
            configure_pigeonhole(config, "that_fixture")
    """

    return make_runner(testdir, conftest, call_args)


def test_no_fixture_used_pass(that_runner):
    py_file = """
        def test_no_fixture_used_pass():
            pass
        """
    expected_lines = [
        "collected 1 item",
        "*===== Pigeonhole: that_fixture =====*",
        " N/A | p: 1    | f: 0    | s: 0    | e: 0*",
    ]
    expected_outcomes = {'passed': 1, 'failed': 0, 'skipped': 0, 'error': 0}
    that_runner(py_file, expected_lines, expected_outcomes)


def test_no_fixture_used_fail(that_runner):
    py_file = """
        def test_no_fixture_used_fail():
            assert 0
        """
    expected_lines = [
        "collected 1 item",
        "*===== Pigeonhole: that_fixture =====*",
        " N/A | p: 0    | f: 1    | s: 0    | e: 0*",
    ]
    expected_outcomes = {'passed': 0, 'failed': 1, 'skipped': 0, 'error': 0}
    that_runner(py_file, expected_lines, expected_outcomes)


def test_no_fixture_used_side_error(that_runner):
    py_file = """
        import pytest

        @pytest.fixture
        def side_error(int_fixture):
            assert int_fixture

        def test_no_fixture_used_side_error(side_error):
            assert False
        """
    expected_lines = [
        "collected 3 items",
        "*===== Pigeonhole: that_fixture =====*",
        " N/A | p: 0    | f: 2    | s: 0    | e: 1*",
    ]
    expected_outcomes = {'passed': 0, 'failed': 2, 'skipped': 0, 'error': 1}
    that_runner(py_file, expected_lines, expected_outcomes)


def test_no_fixture_used_params_pass(that_runner):
    py_file = """
        def test_no_fixture_used_params_side_error(int_fixture):
            pass
        """
    expected_lines = [
        "collected 3 items",
        "*===== Pigeonhole: that_fixture =====*",
        " N/A | p: 3    | f: 0    | s: 0    | e: 0*",
    ]
    expected_outcomes = {'passed': 3, 'failed': 0, 'skipped': 0, 'error': 0}
    that_runner(py_file, expected_lines, expected_outcomes)


def test_no_fixture_used_params_fail(that_runner):
    py_file = """
        def test_no_fixture_used_params_fail(int_fixture):
            assert int_fixture
        """
    expected_lines = [
        "collected 3 items",
        "*===== Pigeonhole: that_fixture =====*",
        " N/A | p: 2    | f: 1    | s: 0    | e: 0*",
    ]
    expected_outcomes = {'passed': 2, 'failed': 1, 'skipped': 0, 'error': 0}
    that_runner(py_file, expected_lines, expected_outcomes)


def test_no_fixture_used_forward_error(that_runner):
    py_file = """
        import pytest

        @pytest.fixture
        def forward_error(that_fixture):
            assert that_fixture

        def test_no_fixture_used_forward_error(forward_error):
            pass
        """
    expected_lines = [
        "collected 4 items",
        "*===== Pigeonhole: that_fixture =====*",
        " 'one' | p: 1    | f: 0    | s: 0    | e: 0*",
        " 'two' | p: 1    | f: 0    | s: 0    | e: 0*",
        " 23    | p: 1    | f: 0    | s: 0    | e: 0*",
        " False | p: 0    | f: 0    | s: 0    | e: 1*",
    ]
    expected_outcomes = {'passed': 3, 'failed': 0, 'skipped': 0, 'error': 1}
    that_runner(py_file, expected_lines, expected_outcomes)


def test_no_fixture_used_collection_skip_forward_error(that_runner):

    py_file = """
        import pytest

        @pytest.fixture
        def forward_error(that_fixture):
            assert that_fixture

        @pytest.mark.skip(reason="good reason")
        def test_no_fixture_used_collection_skip_forward_error(forward_error):
            pass
        """

    expected_lines = [
        "collected 4 items",
        "*===== Pigeonhole: that_fixture =====*",
        " 'one' | p: 0    | f: 0    | s: 1    | e: 0*",
        " 'two' | p: 0    | f: 0    | s: 1    | e: 0*",
        " 23    | p: 0    | f: 0    | s: 1    | e: 0*",
        " False | p: 0    | f: 0    | s: 1    | e: 0*",
    ]
    expected_outcomes = {'passed': 0, 'failed': 0, 'skipped': 4, 'error': 0}
    that_runner(py_file, expected_lines, expected_outcomes)


def test_no_fixture_used_runtime_skip_forward_error(that_runner):
    py_file = """
        import pytest

        @pytest.fixture
        def forward_error(that_fixture):
            assert that_fixture

        def test_no_fixture_used_collection_skip_forward_error(forward_error):
            pytest.skip(reason="good reason")
        """

    expected_lines = [
        "collected 4 items",
        "*===== Pigeonhole: that_fixture =====*",
        " 'one' | p: 0    | f: 1    | s: 0    | e: 0*",
        " 'two' | p: 0    | f: 1    | s: 0    | e: 0*",
        " 23    | p: 0    | f: 1    | s: 0    | e: 0*",
        " False | p: 0    | f: 0    | s: 0    | e: 1*",
    ]
    expected_outcomes = {'passed': 0, 'failed': 3, 'skipped': 0, 'error': 1}
    that_runner(py_file, expected_lines, expected_outcomes)


def test_fixture_used_collection_xfail(that_runner):
    py_file = """
        import pytest

        @pytest.mark.xfail
        def test_fixture_used_collection_xfail(that_fixture):
            assert not that_fixture
        """

    expected_lines = [
        "collected 4 items",
        "*===== Pigeonhole: that_fixture =====*",
        " 'one' | p: 0    | f: 0    | s: 1    | e: 0*",
        " 'two' | p: 0    | f: 0    | s: 1    | e: 0*",
        " 23    | p: 0    | f: 0    | s: 1    | e: 0*",
        " False | p: 1    | f: 0    | s: 0    | e: 0*",
    ]
    expected_outcomes = {'passed': 0, 'failed': 0, 'skipped': 0, 'error': 0}
    that_runner(py_file, expected_lines, expected_outcomes)


def test_fixture_used_collection_xfail_side_error(that_runner):
    py_file = """
        import pytest

        @pytest.fixture
        def side_error(int_fixture):
            assert int_fixture

        @pytest.mark.xfail
        def test_fixture_used_collection_xfail_side_error(that_fixture, side_error):
            assert not that_fixture
        """

    expected_lines = [
        "collected 12 items",
        "*===== Pigeonhole: that_fixture =====*",
        " 'one' | p: 0    | f: 0    | s: 3    | e: 0*",
        " 'two' | p: 0    | f: 0    | s: 3    | e: 0*",
        " 23    | p: 0    | f: 0    | s: 3    | e: 0*",
        " False | p: 2    | f: 0    | s: 1    | e: 0*",
    ]
    expected_outcomes = {'passed': 0, 'failed': 0, 'skipped': 0, 'error': 0}
    that_runner(py_file, expected_lines, expected_outcomes)


def test_fixture_used_pass(that_runner):
    py_file = """
        def test_fixture_used_pass(that_fixture):
            pass
        """
    expected_lines = [
        "collected 4 items",
        "*===== Pigeonhole: that_fixture =====*",
        " 'one' | p: 1    | f: 0    | s: 0    | e: 0*",
        " 'two' | p: 1    | f: 0    | s: 0    | e: 0*",
        " 23    | p: 1    | f: 0    | s: 0    | e: 0*",
        " False | p: 1    | f: 0    | s: 0    | e: 0*",
    ]
    expected_outcomes = {'passed': 4, 'failed': 0, 'skipped': 0, 'error': 0}
    that_runner(py_file, expected_lines, expected_outcomes)


def test_fixture_used_fail(that_runner):
    py_file = """
        def test_fixture_used_fail(that_fixture):
            assert that_fixture
        """
    expected_lines = [
        "collected 4 items",
        "*===== Pigeonhole: that_fixture =====*",
        " 'one' | p: 1    | f: 0    | s: 0    | e: 0*",
        " 'two' | p: 1    | f: 0    | s: 0    | e: 0*",
        " 23    | p: 1    | f: 0    | s: 0    | e: 0*",
        " False | p: 0    | f: 1    | s: 0    | e: 0*",
    ]
    expected_outcomes = {'passed': 3, 'failed': 1, 'skipped': 0, 'error': 0}
    that_runner(py_file, expected_lines, expected_outcomes)


def test_fixture_used_side_error(that_runner):
    py_file = """
        import pytest

        @pytest.fixture
        def side_error():
            assert False

        def test_fixture_used_side_error(that_fixture, side_error):
            pass
        """
    expected_lines = [
        "collected 4 items",
        "*===== Pigeonhole: that_fixture =====*",
        " 'one' | p: 0    | f: 0    | s: 0    | e: 1*",
        " 'two' | p: 0    | f: 0    | s: 0    | e: 1*",
        " 23    | p: 0    | f: 0    | s: 0    | e: 1*",
        " False | p: 0    | f: 0    | s: 0    | e: 1*",
    ]
    expected_outcomes = {'passed': 0, 'failed': 0, 'skipped': 0, 'error': 4}
    that_runner(py_file, expected_lines, expected_outcomes)


def test_fixture_used_forward_error(that_runner):
    py_file = """
        import pytest

        @pytest.fixture
        def forward_error(that_fixture):
            assert that_fixture

        def test_fixture_used_forward_error(that_fixture, forward_error):
            pass
        """
    expected_lines = [
        "collected 4 items",
        "*===== Pigeonhole: that_fixture =====*",
        " 'one' | p: 1    | f: 0    | s: 0    | e: 0*",
        " 'two' | p: 1    | f: 0    | s: 0    | e: 0*",
        " 23    | p: 1    | f: 0    | s: 0    | e: 0*",
        " False | p: 0    | f: 0    | s: 0    | e: 1*",
    ]
    expected_outcomes = {'passed': 3, 'failed': 0, 'skipped': 0, 'error': 1}
    that_runner(py_file, expected_lines, expected_outcomes)


def test_skips(that_runner):

    py_file = """
        import pytest

        @pytest.fixture(params=["parameter 1", "another parameter value"])
        def that_fixture(request):
            return request.param

        def test_no_fixture():
            pass

        def test_pass(that_fixture):
            pass

        def test_that_fails(that_fixture):
            assert 0

        def test_that_fails_with_msg(that_fixture):
            assert 0, "That's an assertion error."

        @pytest.mark.xfail
        def test_xfailing(that_fixture):
            assert 0, "That's an assertion error."

        @pytest.mark.skip
        def test_skippin(that_fixture):
            assert 0, "That's an assertion error."

        @pytest.mark.skip(reason="It's very important reason")
        def test_skippin_with_reason(that_fixture):
            assert 0, "That's an assertion error."
        """

    expected_lines = [
        "collected 13 items",
        "*===== Pigeonhole: that_fixture =====*",
        " 'another parameter value' | p: 1    | f: 2    | s: 3    | e: 0*",
        " 'parameter 1'             | p: 1    | f: 2    | s: 3    | e: 0*",
        " N/A                       | p: 1    | f: 0    | s: 0    | e: 0*",
    ]

    expected_outcomes = {'passed': 3, 'failed': 4, 'skipped': 4, 'error': 0}
    that_runner(py_file, expected_lines, expected_outcomes)
