import pytest


EXAMPLE_CONFTEST = """
    import pytest

    @pytest.fixture(params=["one", "two", 23, False])
    def that_fixture(request):
        return request.param

    @pytest.fixture(params=range(3))
    def int_fixture(request):
        return request.param

"""
EXAMPLE_PY_FILE = """
    import pytest

    @pytest.fixture
    def faulty_setup(that_fixture):
        assert not that_fixture

    def test_passes(that_fixture):
        pass

    def test_int_passes(int_fixture):
        if not int_fixture:
            pytest.skip()

    def test_fail(that_fixture, int_fixture):
        assert int_fixture

    def test_errors(faulty_setup):
        pass

"""


@pytest.mark.parametrize("fixture_name, lines_to_match", [
    ("that_fixture", [
        "*=========== Pigeonhole: that_fixture =======*",
        " 'one' | p: 3    | f: 1    | s: 0    | e: 1  *",
        " 'two' | p: 3    | f: 1    | s: 0    | e: 1  *",
        " 23    | p: 3    | f: 1    | s: 0    | e: 1  *",
        " False | p: 4    | f: 1    | s: 0    | e: 0  *",
        " N/A   | p: 2    | f: 0    | s: 1    | e: 0  *",
    ]),
    ("int_fixture", [
        "*=========== Pigeonhole: int_fixture ========*",
        " 0   | p: 0    | f: 4    | s: 1    | e: 0  *",
        " 1   | p: 5    | f: 0    | s: 0    | e: 0  *",
        " 2   | p: 5    | f: 0    | s: 0    | e: 0  *",
        " N/A | p: 5    | f: 0    | s: 0    | e: 3  *",
    ]),
])
def test_usage(testdir, fixture_name, lines_to_match):

    testdir.makeconftest(EXAMPLE_CONFTEST)
    testdir.makepyfile(EXAMPLE_PY_FILE)
    result = testdir.runpytest("-q", "--pigeonhole", fixture_name)
    result.stdout.fnmatch_lines(lines_to_match)
